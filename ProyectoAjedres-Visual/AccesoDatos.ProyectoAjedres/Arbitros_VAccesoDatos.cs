﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ProyectoAjedres;

namespace AccesoDatos.ProyectoAjedres
{
    public class Arbitros_VAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public  Arbitros_VAccesoDatos()
    {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedres", 3306);
    }
        
        public List< Arbitros_V> GetArbitros_Vs(string filtro)
        {

            var listArbitros_vs = new List<Arbitros_V>();
            var ds = new DataSet();
            string consulta = "select * from Arbitros_VAccesoDatos where nombreA like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "Arbitros_VAccesoDatos");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var Arbitros = new Arbitros_V
                {
                  
                     NombreA= row["nombreA"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString(),
                    Campeonatos = Convert.ToInt32(row["campeonatos"]),
                    Nombrep = Convert.ToInt32(row["nombreP"]),
                };
                listArbitros_vs.Add(Arbitros);
            }
            return listArbitros_vs;
           }
    }
}

