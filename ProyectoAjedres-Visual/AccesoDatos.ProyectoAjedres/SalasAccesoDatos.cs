﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using System.Data;

namespace AccesoDatos.ProyectoAjedres
{
     public class SalasAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public SalasAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedres", 3306);
        }
        public void Guardar(Salas salas)
        {
            if (salas.idSala == 0)
            {
                string consulta = string.Format("Insert into salas values(null,'{0}','{1}','{2}','{3}')",
                    salas.numSala, salas.capacidad, salas.medios, salas.fkidHotel);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update salas set numSala = '{0}', Capacidad = '{1}', medios = '{2}', fkidHotel = '{3}' where idSala = {4}",
                 salas.numSala, salas.capacidad, salas.medios, salas.fkidHotel, salas.idSala);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idSala)
        {
            string consulta = string.Format("Delete from salas where idSala = {0} ", idSala);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Salas> GetSalas(string filtro)
        {
            var listsalas = new List<Salas>();
            var ds = new DataSet();
            string consulta = "select * from salas where numSala like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "salas");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var salas = new Salas
                {
                    idSala = Convert.ToInt32(row["idSala"]),
                    numSala = row["numSala"].ToString(),
                    capacidad = Convert.ToInt32(row["Capacidad"]),
                    medios = row["medios"].ToString(),
                    fkidHotel = Convert.ToInt32(row["fkidHotel"])
                };
                listsalas.Add(salas);
            }
            return listsalas;
        }
    }
}
