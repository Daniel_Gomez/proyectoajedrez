﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using System.Data;
using System.Windows.Forms;

namespace AccesoDatos.ProyectoAjedres
{
    public class ArbitrosAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public ArbitrosAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedres", 3306);
        }
        public void Guardar(Arbitros arbitros)
        {
            if (arbitros.idArbitro == 0)
            {
                try
                {
                    string consulta = string.Format("Insert into arbitros values(null,'{0}','{1}','{2}','{3}','{4}')",
                        arbitros.nombreA, arbitros.direccion, arbitros.telefono, arbitros.campeonato, arbitros.fkidPais);
                    conexion.EjecutarConsulta(consulta);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "error");
                }
            }
            else
            {
                string consulta = string.Format("Update arbitros set nombreA = '{0}', direccion = '{1}', telefono = '{2}', campeonatos = '{3}', fkidPais = '{4}' where idArbitro = {5}",
                    arbitros.nombreA, arbitros.direccion, arbitros.telefono, arbitros.campeonato, arbitros.fkidPais, arbitros.idArbitro);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idArbitro)
        {
            string consulta = string.Format("Delete from arbitros where idArbitro = {0} ", idArbitro);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Arbitros> GetArbitros(string filtro)
        {
            var listarbitros = new List<Arbitros>();
            var ds = new DataSet();
            string consulta = "select * from arbitros where nombreA like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "arbitros");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var arbitros = new Arbitros
                {
                    idArbitro = Convert.ToInt32(row["idArbitro"]),
                    nombreA = row["nombreA"].ToString(),
                    direccion = row["direccion"].ToString(),
                    telefono = row["telefono"].ToString(),
                    campeonato = Convert.ToInt32(row["campeonatos"]),
                    fkidPais = Convert.ToInt32(row["fkidPais"])
                };
                listarbitros.Add(arbitros);
                }
            return listarbitros;
        }
    }
}
