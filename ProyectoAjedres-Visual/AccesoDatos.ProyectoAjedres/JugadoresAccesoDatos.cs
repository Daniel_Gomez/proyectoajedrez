﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using System.Data;

namespace AccesoDatos.ProyectoAjedres
{
     public class JugadoresAccesoDatos
    {
        ConexionAccesoDatos conexion;
        
            public JugadoresAccesoDatos()
            {
                conexion = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedres", 3306);
            }
            public void Guardar(Jugadores jugadores)
            {
            if (jugadores.idJugador == 0)
            {
                string consulta = string.Format("insert into jugadores values(null,'{0}','{1}','{2}','{3}','{4}','{5}')",
                    jugadores.nombreJ, jugadores.direccion, jugadores.telefono, jugadores.Campeonatos, jugadores.nivel, jugadores.fkidPais);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update jugadores set nombreJ = '{0}', direccion = '{1}', telefono = '{2}', campeonatos = '{3}', nivel = '{4}', fkidPais = '{5}' where idJugador = {6}",
                    jugadores.nombreJ, jugadores.direccion, jugadores.telefono, jugadores.Campeonatos, jugadores.nivel, jugadores.fkidPais, jugadores.idJugador);
                    conexion.EjecutarConsulta(consulta);
                }
            }
            public void Eliminar(int idJugador)
            {
                string consulta = string.Format("Delete from jugadores where idJugador = {0} ", idJugador);
                conexion.EjecutarConsulta(consulta);
            }
            public List<Jugadores> GetJugadores(string filtro)
            {
                var listjugadores = new List<Jugadores>();
                var ds = new DataSet();
                string consulta = "select * from jugadores where nombreJ like '%" + filtro + "%'";
                ds = conexion.ObtenerDatos(consulta, "jugadores");

                var dt = new DataTable();
                dt = ds.Tables[0];

                foreach (DataRow row in dt.Rows)
                {
                    var jugadores = new Jugadores
                    {
                        idJugador = Convert.ToInt32(row["idJugador"]),
                        nombreJ = row["nombreJ"].ToString(),
                        direccion = row["direccion"].ToString(),
                        telefono = row["telefono"].ToString(),
                        Campeonatos = Convert.ToInt32(row["campeonatos"]),
                        nivel = Convert.ToInt32(row["nivel"]),
                        fkidPais = Convert.ToInt32(row["fkidPais"])
                    };
                    listjugadores.Add(jugadores);
                }
                return listjugadores;
            }
        }
    }
