﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using System.Data;

namespace AccesoDatos.ProyectoAjedres
{
      public class HotelesAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public HotelesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedres", 3306);
        }
        public void Guardar(Hoteles hoteles)
        {
            if (hoteles.idHotel == 0)
            {
                string consulta = string.Format("Insert into hoteles values(null,'{0}','{1}','{2}')",
                    hoteles.nombreH, hoteles.direccion, hoteles.telefono);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update hoteles set nombreH = '{0}', direccion = '{1}', telefono = '{2}' where idHotel = {3}",
                 hoteles.nombreH, hoteles.direccion, hoteles.telefono, hoteles.idHotel);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idHotel)
        {
            string consulta = string.Format("Delete from hoteles where idHotel = {0} ", idHotel);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Hoteles> GetHoteles(string filtro)
        {
            var listhoteles = new List<Hoteles>();
            var ds = new DataSet();
            string consulta = "select * from hoteles where nombreH like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "hoteles");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var hotel = new Hoteles
                {
                    idHotel = Convert.ToInt32(row["idHotel"]),
                    nombreH = row["nombreH"].ToString(),
                    direccion = row["direccion"].ToString(),
                    telefono = row["telefono"].ToString()
                };
                listhoteles.Add(hotel);
            }
            return listhoteles;
        }
    }
}

