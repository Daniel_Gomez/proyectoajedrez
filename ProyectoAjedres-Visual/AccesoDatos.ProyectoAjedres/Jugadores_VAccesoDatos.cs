﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using System.Data;

namespace AccesoDatos.ProyectoAjedres
{
    public class Jugadores_VAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public Jugadores_VAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedres", 3306);
        }
        public List<Jugadores_V> GetJugadores_Vs(string filtro)
        {
            var listJugadores_Vs = new List<Jugadores_V>();
            var ds = new DataSet();
            string consulta = "select * from Jugadores_VAccesoDatos where nombreJ like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "Jugadores_VAccesoDatos");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var Jugadores = new Jugadores_V
                {
                    NombreJ = row["nombreJ"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString(),
                    Campeonatos= Convert.ToInt32(row["campeonatos"]),
                    NombreP= Convert.ToInt32(row["nombreP"]),
                };
                listJugadores_Vs.Add(Jugadores);
            }
            return listJugadores_Vs;
        }
    }
}

