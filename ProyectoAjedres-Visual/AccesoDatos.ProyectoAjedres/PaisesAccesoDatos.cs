﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using System.Data;

namespace AccesoDatos.ProyectoAjedres
{
     public class PaisesAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public PaisesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedres", 3306);
        }
        public void Guardar(Paises paises)
        {
            if (paises.IDPais == 0)
            {
                string consulta = string.Format("insert into paises values(null,'{0}','{1}')",
                    paises.NombreP, paises.NumClubes);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update paises set nombreP = '{0}', numClubes = '{1}' where idPais = {2}",
                    paises.NombreP, paises.NumClubes, paises.IDPais);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int IDPais)
        {
            string consulta = string.Format("Delete from paises where idPais = {0} ", IDPais);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Paises> GetPaises(string filtro)
        {
            var listpaises = new List<Paises>();
            var ds = new DataSet();
            string consulta = "select * from paises where idPais like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "paises");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var paises = new Paises
                {
                    IDPais = Convert.ToInt32(row["idPais"]),
                    NombreP = row["nombreP"].ToString(),
                    NumClubes = Convert.ToInt32(row["numClubes"])
                };
                listpaises.Add(paises);
            }
            return listpaises;
        }
    }
}

