﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using System.Data;

namespace AccesoDatos.ProyectoAjedres
{
    public class PartidasAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public PartidasAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "ClubAjedres", 3306);
        }
        public void Guardar(Partidas partidas)
        {
            if (partidas.CodigoP == 0)
            {
                string consulta = string.Format("insert into partidas values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}')",
                    partidas.Jornada, partidas.EntradasVendidas, partidas.NumMovimientos, partidas.fkidJugador, partidas.fkidJugador2, partidas.fkidArbitro, partidas.fkidSala);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update partidas set jornada = '{0}', entradasvendidas = '{1}', numeromovimientos = '{2}', fkidJugador = '{3}', fkidJugador2 = '{4}', fkidArbitro = '{5}', fkidSala = '{6}' where codigoP = {7}",
                 partidas.Jornada, partidas.EntradasVendidas, partidas.NumMovimientos, partidas.fkidJugador, partidas.fkidJugador2, partidas.fkidArbitro, partidas.fkidSala, partidas.CodigoP);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int CodigoP)
        {
            string consulta = string.Format("Delete from partidas where codigoP = {0} ", CodigoP);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Partidas>GetPartidas(string filtro)
        {
            var listpartidas = new List<Partidas>();
            var ds = new DataSet();
            string consulta = "select * from partidas where jornada like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "partidas");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var partidas = new Partidas
                {
                    CodigoP = Convert.ToInt32(row["codigoP"]),
                    Jornada = row["jornada"].ToString(),
                    EntradasVendidas = Convert.ToInt32(row["entradasvendidas"]),
                    NumMovimientos = Convert.ToInt32(row["numeromovimientos"]),
                    fkidJugador = Convert.ToInt32(row["fkidJugador"]),
                    fkidJugador2 = Convert.ToInt32(row["fkidJugador2"]),
                    fkidArbitro = Convert.ToInt32(row["fkidArbitro"]),
                    fkidSala = Convert.ToInt32(row["fkidSala"])
                };
                listpartidas.Add(partidas);
            }
            return listpartidas;
        }
    }
}