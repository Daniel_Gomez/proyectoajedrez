﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using MySql.Data;
using System.Data;
namespace AccesoDatos.ProyectoAjedres
{
    public class ConexionAccesoDatos
    {
        private MySqlConnection con;

        public ConexionAccesoDatos(string servidor, string usuario, string password, string database, uint puerto)
        {
            MySqlConnectionStringBuilder cadenaConexion = new MySqlConnectionStringBuilder();
            cadenaConexion.Server = servidor;
            cadenaConexion.UserID = usuario;
            cadenaConexion.Password = password;
            cadenaConexion.Database = database;
            cadenaConexion.Port = puerto;
            con = new MySqlConnection(cadenaConexion.ToString());
        }
        public void EjecutarConsulta(string consulta)
        {
            con.Open();
            var command = new MySqlCommand(consulta, con);
            command.ExecuteNonQuery();
            con.Close();
        }
        public DataSet ObtenerDatos(string consulta, string tabla)
        {
            var ds = new DataSet();
            MySqlDataAdapter da= new MySqlDataAdapter(consulta, con);
            da.Fill(ds, tabla);
            return ds;
        }
    }
}