﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ProyectoAjedres
{
      public class Hoteles
    {
        private int _IDHotel;
        private string _NombreH;
        private string _Direccion;
        private string _Telefono;

        public int idHotel { get => _IDHotel; set => _IDHotel = value; }
        public string nombreH { get => _NombreH; set => _NombreH = value; }
        public string direccion { get => _Direccion; set => _Direccion = value; }
        public string telefono { get => _Telefono; set => _Telefono = value; }
    }
}
