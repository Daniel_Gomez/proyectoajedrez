﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ProyectoAjedres
{
    public class Arbitros
    {
        private int _IDArbitro;
        private string _NombreA;
        private string _Direccion;
        private string _Telefono;
        private int _Campeonatos;
        private int _FKIDPais;
        public int idArbitro { get => _IDArbitro; set => _IDArbitro = value; }
        public string nombreA { get => _NombreA; set => _NombreA = value; }
        public string direccion { get => _Direccion; set => _Direccion = value; }
        public string telefono { get => _Telefono; set => _Telefono = value; }
        public int campeonato { get => _Campeonatos; set => _Campeonatos = value; }
        public int fkidPais { get => _FKIDPais; set => _FKIDPais = value; }

    }
}
