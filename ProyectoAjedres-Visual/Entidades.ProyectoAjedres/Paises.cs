﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ProyectoAjedres
{
      public class Paises
    {
        private int _IDPais;
        private string _NombreP;
        private int _NumClubes;
        public int IDPais { get => _IDPais; set => _IDPais = value; }
        public string NombreP { get => _NombreP; set => _NombreP = value; }
        public int NumClubes { get => _NumClubes; set => _NumClubes = value; }
    }
}
