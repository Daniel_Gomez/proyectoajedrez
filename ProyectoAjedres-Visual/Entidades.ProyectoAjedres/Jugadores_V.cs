﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ProyectoAjedres
{
    public class Jugadores_V
    {
        private string _nombreJ;
        private string _direccion;
        private string _telefono;
        private int _campeonatos;
        private int _nivel;
        private int _nombreP;

        public string NombreJ { get => _nombreJ; set => _nombreJ = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public int Campeonatos { get => _campeonatos; set => _campeonatos = value; }
        public int Nivel { get => _nivel; set => _nivel = value; }
        public int NombreP { get => _nombreP; set => _nombreP = value; }
    }
}
