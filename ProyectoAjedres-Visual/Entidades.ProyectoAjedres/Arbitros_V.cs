﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ProyectoAjedres
{
    public class Arbitros_V
    {
        private string _nombreA;
        private string _direccion;
        private string _telefono;
        private int _campeonatos;
        private int _nombrep;

        public string NombreA { get => _nombreA; set => _nombreA = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public int Campeonatos { get => _campeonatos; set => _campeonatos = value; }
        public int Nombrep { get => _nombrep; set => _nombrep = value; }
    }
}
