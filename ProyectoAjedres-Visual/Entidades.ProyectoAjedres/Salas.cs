﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ProyectoAjedres
{
      public class Salas
    {
        private int _IDSala;
        private string _NumSala;
        private int _Capacidad;
        private string _Medios;
        private int _FKIDHotel;
        public int idSala { get => _IDSala; set => _IDSala = value; }
        public string numSala { get => _NumSala; set => _NumSala = value; }
        public int capacidad { get => _Capacidad; set => _Capacidad = value; }
        public string medios { get => _Medios; set => _Medios = value; }
        public int fkidHotel { get => _FKIDHotel; set => _FKIDHotel = value; }
    }
}
