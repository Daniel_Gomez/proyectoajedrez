﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using AccesoDatos.ProyectoAjedres;

namespace LogicaNegocio.ProyectoAjedres
{
      public class PartidasManejador
    {
        private PartidasAccesoDatos _partidasAccesoDatos = new PartidasAccesoDatos();
        public void Guardar(Partidas partidas)
        {
            _partidasAccesoDatos.Guardar(partidas);
        }
        public void Eliminar(int codigoP)
        {
            _partidasAccesoDatos.Eliminar(codigoP);
        }
        public List<Partidas> GetPartidas(string filtro)
        {
            var Listpartidas = _partidasAccesoDatos.GetPartidas(filtro);
            return Listpartidas;
        }
        public Tuple<bool, string> validarPartidas(Partidas partidas)  /*para validar */
        {
            bool valido = true;
            string mensaje = "";

            if (partidas.Jornada == "")
            {
                mensaje = mensaje + "la jornada es necesaria \n";

                valido = false;
            }
            if (partidas.EntradasVendidas == 0)
            {
                mensaje = mensaje + "el numero de entradas vendidas es necesario \n";
                valido = false;
            }
            if (partidas.NumMovimientos == 0)
            {
                mensaje = mensaje + "el numero de movimientos son necesarios \n";
            }
            if (partidas.fkidJugador == 0)
            {
                mensaje = mensaje + "el jugador 1 es necesario \n";
                valido = false;
            }
            if (partidas.fkidJugador2 == 0)
            {
                mensaje = mensaje + "el jugador 2 es necesario \n";
                valido = false;
            }
            if (partidas.fkidArbitro == 0)
            {
                mensaje = mensaje + "el arbitro es necesario \n";
                valido = false;
            }
            if (partidas.fkidSala == 0)
            {
                mensaje = mensaje + "la sala del hotel es necesaria";
                valido = false;
            }
            return Tuple.Create(valido, mensaje);
        }
    }
}
