﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using AccesoDatos.ProyectoAjedres;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ProyectoAjedres
{
      public class HotelesManejador
    {
        private HotelesAccesoDatos _hotelesAccesoDatos = new HotelesAccesoDatos();
        public void Guardar( Hoteles hoteles)
        {
            _hotelesAccesoDatos.Guardar(hoteles);
        }
        public void Eliminar(int idHotel)
        {
            _hotelesAccesoDatos.Eliminar(idHotel);
        }
        public List<Hoteles> GetHoteles(string Filtro)
        {
            var Listhoteles = _hotelesAccesoDatos.GetHoteles(Filtro);
            return Listhoteles;
        }
        public Tuple<bool, string> validarhoteles(Hoteles hoteles)  /*para validar */
        {
            bool valido = true;
            string mensaje = "";

            if (hoteles.idHotel == 0)
            {
                mensaje = mensaje + "el ID del Hotel es necesario \n";
                valido = false;
            }
            if (hoteles.nombreH == "")
            {
                mensaje = mensaje + "el Nombre del Hotel es necesario \n";

                valido = false;
            }
            if (hoteles.direccion == "")
            {
                mensaje = mensaje + "la direccion del hotel es necesaria \n";
                valido = false;
            }
            if (hoteles.telefono == "")
            {
                mensaje = mensaje + "el telefono del Hotel es necesario \n";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }
    }
}
