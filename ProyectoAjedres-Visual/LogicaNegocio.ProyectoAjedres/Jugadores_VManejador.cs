﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.ProyectoAjedres;
using Entidades.ProyectoAjedres;


namespace LogicaNegocio.ProyectoAjedres
{
    public class Jugadores_VManejador
    {
        private Jugadores_VAccesoDatos jugadores_VAccesoDatos = new Jugadores_VAccesoDatos();

        public List<Jugadores_V> GetJugadores_Vs(string filtro)
        {
            var listJugadores = jugadores_VAccesoDatos.GetJugadores_Vs(filtro);
            return listJugadores;
        }
   }
}
