﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using AccesoDatos.ProyectoAjedres;

namespace LogicaNegocio.ProyectoAjedres
{
   public class Arbitros_VManejador
    {
        private Arbitros_VAccesoDatos Arbitros_VAccesoDatos = new Arbitros_VAccesoDatos();

        public List<Arbitros_V> GetArbitros_Vs(string filtro)
        {
            var listArbitros = Arbitros_VAccesoDatos.GetArbitros_Vs(filtro);
            return listArbitros;
        }
  }
}
