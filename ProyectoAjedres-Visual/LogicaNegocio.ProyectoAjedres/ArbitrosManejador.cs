﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using AccesoDatos.ProyectoAjedres;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ProyectoAjedres
{
      public class ArbitrosManejador
    {
        private ArbitrosAccesoDatos _arbitrosAccesoDatos = new ArbitrosAccesoDatos();
        public void Guardar(Arbitros arbitros)
        {
            _arbitrosAccesoDatos.Guardar(arbitros);
        }
        public void Eliminar(int idArbitro)
        {
            _arbitrosAccesoDatos.Eliminar(idArbitro);
        }
        public List<Arbitros> GetArbitros(string filtro)
        {
            var Listarbitros = _arbitrosAccesoDatos.GetArbitros(filtro);
            return Listarbitros;
        }
        public Tuple<bool, string> validararbitros(Arbitros arbitros)  /*para validar */
        {
            bool valido = true;
            string mensaje = "";
            
            if (arbitros.nombreA == "")
            {
                mensaje = mensaje + "el nombre del Arbitro es necesario \n";

                valido = false;
            }
            if (arbitros.direccion == "")
            {
                mensaje = mensaje + "la direccion del Arbitro es necesaria \n";
                valido = false;
            }
            if (arbitros.telefono == "")
            {
                mensaje = mensaje + "el numero de telefono del Arbitro es necesario \n";
                valido = false;
            }
            if (arbitros.campeonato == 0)
            {
                mensaje = mensaje + "el numero de campeonatos son necesarios \n";
                valido = false;
            }
            if (arbitros.fkidPais == 0)
            {
                mensaje = mensaje + "el pais del Arbitro es necesario \n";
                valido = false;
            }
            return Tuple.Create(valido, mensaje);
        }
    }
}
