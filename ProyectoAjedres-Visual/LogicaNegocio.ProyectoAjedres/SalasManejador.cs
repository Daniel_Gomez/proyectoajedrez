﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using AccesoDatos.ProyectoAjedres;

namespace LogicaNegocio.ProyectoAjedres
{
    public class SalasManejador
    {
        private SalasAccesoDatos _salasAccesoDatos = new SalasAccesoDatos();
        public void Guardar(Salas salas)
        {
            _salasAccesoDatos.Guardar(salas);
        }
        public void Eliminar(int idSala)
        {
            _salasAccesoDatos.Eliminar(idSala);
        }
        public List<Salas> GetSalas(string filtro)
        {
            var listsalas = _salasAccesoDatos.GetSalas(filtro);
            return listsalas;
        }
        public Tuple<bool, string> validarsalas(Salas salas)  /*para validar */
        {
            bool valido = true;
            string mensaje = "";
            if (salas.numSala == "")
            {
                mensaje = mensaje + "el numero de la Sala es necesario \n";

                valido = false;
            }
            if (salas.capacidad == 0)
            {
                mensaje = mensaje + "la capacidad de la Sala es necesaria \n";
                valido = false;
            }
            if (salas.medios == "")
            {
                mensaje = mensaje + "los medios a utilizar de la sala son necesarios \n";
                valido = false;
            }
            if (salas.fkidHotel == 0)
            {
                mensaje = mensaje + "el hotel es necesario \n";
                valido = false;
            }
            return Tuple.Create(valido, mensaje);
        }
    }
}
