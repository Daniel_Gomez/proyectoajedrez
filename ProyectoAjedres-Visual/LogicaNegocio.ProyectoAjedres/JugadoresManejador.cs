﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using AccesoDatos.ProyectoAjedres;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ProyectoAjedres
{
     public class JugadoresManejador
    {
        private JugadoresAccesoDatos _jugadoresAccesoDatos = new JugadoresAccesoDatos();
        public void Guardar(Jugadores jugadores)
        {
            _jugadoresAccesoDatos.Guardar(jugadores);
        }
        public void Eliminar(int idJugador)
        {
            _jugadoresAccesoDatos.Eliminar(idJugador);
        }
        public List<Jugadores> GetJugadores(string filtro)
        {
            var Listjugadores = _jugadoresAccesoDatos.GetJugadores(filtro);
            return Listjugadores;
        }
        public Tuple<bool, string> validarjugadores(Jugadores jugadores)  /*para validar */
        {
            bool valido = true;
            string mensaje = "";
            
            if (jugadores.nombreJ == "")
            {
                mensaje = mensaje + "el nombre del jugador es necesario \n";

                valido = false;
            }
            if (jugadores.direccion == "")
            {
                mensaje = mensaje + "la direccion del jugador es necesaria \n";
                valido = false;
            }
            if (jugadores.telefono == "")
            {
                mensaje = mensaje + "el numero de telefono del jugador es necesario \n";
            }
            if (jugadores.Campeonatos == 0)
            {
                mensaje = mensaje + "el numero de campeonatos son necesarios \n";
                valido = false;
            }
            if (jugadores.nivel == 0)
            {
                mensaje = mensaje + "el nivel del Jugador es necesario \n";
                valido = false;
            }
            if (jugadores.fkidPais == 0)
            {
                mensaje = mensaje + "el pais del Jugador es necesario \n";
                valido = false;
            }
            return Tuple.Create(valido, mensaje);
        }
    }
}
