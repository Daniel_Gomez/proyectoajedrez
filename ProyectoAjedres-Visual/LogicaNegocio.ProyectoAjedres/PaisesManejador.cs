﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedres;
using AccesoDatos.ProyectoAjedres;

namespace LogicaNegocio.ProyectoAjedres
{
     public class PaisesManejador
    {
        private PaisesAccesoDatos _paisesAccesoDatos = new PaisesAccesoDatos();
        public void Guardar(Paises paises)
        {
            _paisesAccesoDatos.Guardar(paises);
        }
        public void Eliminar(int idPais)
        {
            _paisesAccesoDatos.Eliminar(idPais);
        }
        public List<Paises> GetPaises(string filtro)
        {
            var Listpaises =_paisesAccesoDatos.GetPaises(filtro);
            return Listpaises;
        }
        public Tuple<bool, string> validarpaises(Paises paises)  /*para validar */
        {
            bool valido = true;
            string mensaje = "";
            
            if (paises.NombreP == "")
            {
                mensaje = mensaje + "el Nombre es necesario \n";

                valido = false;
            }
            if (paises.NumClubes == 0)
            {
                mensaje = mensaje + "el numero de clubes es necesario \n";
                valido = false;
            }
            
            return Tuple.Create(valido, mensaje);
        }
    }
}
