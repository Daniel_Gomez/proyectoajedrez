﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ProyectoAjedres;
using LogicaNegocio.ProyectoAjedres;

namespace ProyectoAjedres
{
    public partial class FrmPaises : Form
    {
        private PaisesManejador _paisesManejador;
        private Paises paises;
        public FrmPaises()
        {
            InitializeComponent();
            _paisesManejador = new PaisesManejador();
            paises = new Paises();
        }
        private void BuscarPais(string Filtro)
        {
            dtgPaises.DataSource = _paisesManejador.GetPaises(Filtro);
        }

        private void FrmPaises_Load(object sender, EventArgs e)
        {
            BuscarPais("");
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }
        private void CargarPais()
        {
            paises.IDPais = Convert.ToInt32(lblContador.Text);
            paises.NombreP = txtNombre.Text;
            paises.NumClubes = Convert.ToInt32(txtNumClubs.Text);
        }
        private void controlarbotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void controlarcuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtNumClubs.Enabled = activar;
        }
        private void limpiarcuadros()
        {
            txtNombre.Text = "";
            txtNumClubs.Text = "";
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este Pais", "Eliminar Pais", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarPais("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void Eliminar()
        {
            var IDPais = dtgPaises.CurrentRow.Cells["idPais"].Value;
            _paisesManejador.Eliminar(Convert.ToInt32(IDPais));
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            txtNombre.Focus();
        }
        private bool validarPais()
        {
            var tupla = _paisesManejador.validarpaises(paises);
            var valido = tupla.Item1;
            var mensaje = tupla.Item2;
            if (!valido)
            {
                MessageBox.Show(mensaje, "error de validacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return valido;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            CargarPais();
            try
            {
                if (validarPais())
                {
                    GuardarPaises();
                    limpiarcuadros();
                    BuscarPais(" ");
                    controlarbotones(true, false, false, true);
                    controlarcuadros(false);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            limpiarcuadros();
        }
        private void GuardarPaises()
        {
            _paisesManejador.Guardar(paises);
            {


            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarPais(txtBuscar.Text);
        }
        private void ModificarPaises()
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            lblContador.Text = dtgPaises.CurrentRow.Cells["idPais"].Value.ToString();
            txtNombre.Text = dtgPaises.CurrentRow.Cells["nombreP"].Value.ToString();
            txtNumClubs.Text = dtgPaises.CurrentRow.Cells["numClubes"].Value.ToString();
        }

        private void dtgPaises_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarPaises();
                BuscarPais(" ");
            }
            catch (Exception es)
            {

                MessageBox.Show(es.Message);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}
