﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoAjedres
{
    public partial class PantallaPrincipal : Form
    {
        public PantallaPrincipal()
        {
            InitializeComponent();
        }

        private void arbitrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
           FrmArbitros  Arbitros = new FrmArbitros();
            Arbitros.ShowDialog();
        }

        private void jugadoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmJugadores Jugadores = new FrmJugadores();
            Jugadores.ShowDialog();
        }

        private void paisesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPaises Paises = new FrmPaises();
            Paises.ShowDialog();
        }

        private void partidasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPartidas Partidas = new FrmPartidas();
            Partidas.ShowDialog();
        }

        private void salasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSalas Salas = new FrmSalas();
            Salas.ShowDialog();
        }

        private void hotelesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmHoteles Hoteles = new FrmHoteles();
            Hoteles.ShowDialog();
        }
    }
}
