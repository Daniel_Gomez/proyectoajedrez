﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ProyectoAjedres;
using LogicaNegocio.ProyectoAjedres;
using Microsoft.Office.Interop.Excel;
using Microsoft.CSharp.RuntimeBinder;

namespace ProyectoAjedres
{
    public partial class FrmSalas : Form
    {
        private HotelesManejador _hotelesManejador;
        private SalasManejador _salasManejador;
        private Salas salas;

        public FrmSalas()
        {
            InitializeComponent();
            salas = new Salas();
            _hotelesManejador = new HotelesManejador();
            _salasManejador = new SalasManejador();
        }
        private void BuscarSala(string Filtro)
        {
            dtgSalas.DataSource = _salasManejador.GetSalas(Filtro);
        }

        private void FrmSalas_Load(object sender, EventArgs e)
        {
            BuscarSala("");
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }
        private void controlarbotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void controlarcuadros(bool activar)
        {
            txtNumSala.Enabled = activar;
            txtCapacidad.Enabled = activar;
            txtMedios.Enabled = activar;
            cmbFkHotel.Enabled = activar;
        }
        private void limpiarcuadros()
        {
            txtNumSala.Text = "";
            txtCapacidad.Text = "";
            txtMedios.Text = "";
            cmbFkHotel.Text = "";
            lblContador.Text = "0"; 
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar esta sala", "Elimirar Sala", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarSala("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void Eliminar()
        {
            var idSala = dtgSalas.CurrentRow.Cells["idSala"].Value;
            _salasManejador.Eliminar(Convert.ToInt32(idSala));
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            txtNumSala.Focus();
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            try
            {
                GuardarSalas();
                limpiarcuadros();
                BuscarSala(" ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            limpiarcuadros();
        }
        private void GuardarSalas()
        {
            _salasManejador.Guardar(new Salas
            {
                idSala = Convert.ToInt32(lblContador.Text),
                numSala = txtNumSala.Text,
                capacidad = int.Parse(txtCapacidad.Text),
                medios = txtMedios.Text,
                fkidHotel = int.Parse(cmbFkHotel.SelectedValue.ToString())

            });
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarSala(txtBuscar.Text);
        }
        private void ModificarSalas()
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            lblContador.Text = dtgSalas.CurrentRow.Cells["idSala"].Value.ToString();
            txtNumSala.Text = dtgSalas.CurrentRow.Cells["numSala"].Value.ToString();
            txtCapacidad.Text = dtgSalas.CurrentRow.Cells["capacidad"].Value.ToString();
            txtMedios.Text = dtgSalas.CurrentRow.Cells["medios"].Value.ToString();
            cmbFkHotel.Text = dtgSalas.CurrentRow.Cells["fkidHotel"].Value.ToString();
        }

        private void dtgSalas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarSalas();
                BuscarSala(" ");
            }
            catch (Exception es)
            {

                MessageBox.Show(es.Message);
            }
        }
        private void extraerHoteles(string Filtro)
        {
            cmbFkHotel.DataSource = _hotelesManejador.GetHoteles(Filtro);
            cmbFkHotel.ValueMember = "idHotel";
            cmbFkHotel.DisplayMember = "nombreH";
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbFkHotel_Click(object sender, EventArgs e)
        {
            extraerHoteles("");
        }
    }
}
