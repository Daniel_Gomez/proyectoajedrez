﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ProyectoAjedres;
using LogicaNegocio.ProyectoAjedres;

namespace ProyectoAjedres
{
    public partial class FrmJugadores : Form
    {
        private Jugadores jugadores;
        private Jugadores_VManejador jugadores_Manejador;
        private PaisesManejador _paisesManejador;
        private JugadoresManejador _jugadoresManejador;
       
        public FrmJugadores()
        {
            InitializeComponent();
            jugadores = new Jugadores();
            _jugadoresManejador = new JugadoresManejador();
            _paisesManejador = new PaisesManejador();
            
 
        }
        private void BuscarJugador(string Filtro)
        {
            dtgJugadores.DataSource = _jugadoresManejador.GetJugadores(Filtro);
        }

        private void FrmJugadores_Load(object sender, EventArgs e)
        {
            BuscarJugador("");
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }
        private void controlarbotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void controlarcuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtDireccion.Enabled = activar;
            txtTelefono.Enabled = activar;
            txtCampeonatos.Enabled = activar;
            txtNivel.Enabled = activar;
            cmbFkPais.Enabled = activar;
        }
        private void limpiarcuadros()
        {
            txtNombre.Text = "";
            txtDireccion.Text = "";
            txtTelefono.Text = "";
            txtCampeonatos.Text = "";
            txtNivel.Text = "";
            cmbFkPais.Text = "";
            lblContador.Text = "0";
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este jugador", "Elimirar jugador", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarJugador("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void Eliminar()
        {
            var idJugador = dtgJugadores.CurrentRow.Cells["idJugador"].Value;
            _jugadoresManejador.Eliminar(Convert.ToInt32(idJugador));
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            txtNombre.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            try
            {
                    GuardarJugadores();
                    limpiarcuadros();
                    BuscarJugador(" ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            limpiarcuadros();
        }
        private void GuardarJugadores()
        {
            _jugadoresManejador.Guardar(new Jugadores
            {
                idJugador = Convert.ToInt32(lblContador.Text),
                nombreJ = txtNombre.Text,
                direccion = txtDireccion.Text,
                telefono = txtTelefono.Text,
                Campeonatos = int.Parse(txtCampeonatos.Text),
                nivel = int.Parse(txtNivel.Text),
                fkidPais = int.Parse(cmbFkPais.SelectedValue.ToString())
            });
         }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarJugador(txtBuscar.Text);
        }
        private void ModificarJugador()
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            lblContador.Text = dtgJugadores.CurrentRow.Cells["idJugador"].Value.ToString();
            txtNombre.Text = dtgJugadores.CurrentRow.Cells["nombreJ"].Value.ToString();
            txtDireccion.Text = dtgJugadores.CurrentRow.Cells["direccion"].Value.ToString();
            txtTelefono.Text = dtgJugadores.CurrentRow.Cells["telefono"].Value.ToString();
            txtCampeonatos.Text = dtgJugadores.CurrentRow.Cells["campeonatos"].Value.ToString();
            txtNivel.Text = dtgJugadores.CurrentRow.Cells["nivel"].Value.ToString();
            cmbFkPais.Text = dtgJugadores.CurrentRow.Cells["fkidPais"].Value.ToString();
        }

        private void dtgJugadores_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarJugador();
                BuscarJugador(" ");
            }
            catch (Exception es)
            {

                MessageBox.Show(es.Message);
            }
        }
        private void extraerPaises(string Filtro)
        {
            cmbFkPais.DataSource = _paisesManejador.GetPaises(Filtro);
            cmbFkPais.ValueMember = "idPais";
            cmbFkPais.DisplayMember = "nombreP";
        }
        private void cmbFkPais_Click_1(object sender, EventArgs e)
        {
            extraerPaises("");
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
