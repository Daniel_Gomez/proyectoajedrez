﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ProyectoAjedres;
using LogicaNegocio.ProyectoAjedres;

namespace ProyectoAjedres
{
    public partial class FrmHoteles : Form
    {
        private HotelesManejador _hotelesManejador;
        private Hoteles hoteles;
        public FrmHoteles()
        {
            InitializeComponent();
            _hotelesManejador = new HotelesManejador();
            hoteles = new Hoteles();
        }
        private void BuscarHotel(string Filtro)
        {
            dtgHoteles.DataSource = _hotelesManejador.GetHoteles(Filtro);
        }

        private void FrmHoteles_Load(object sender, EventArgs e)
        {
            BuscarHotel("");
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }
        private void controlarbotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void controlarcuadros(bool activar)
        {
            txtNombreHotel.Enabled = activar;
            txtDireccion.Enabled = activar;
            txtTelefono.Enabled = activar;
        }
        private void limpiarcuadros()
        {
            txtNombreHotel.Text = "";
            txtDireccion.Text = "";
            txtTelefono.Text = "";
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este hotel", "Elimirar hotel", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarHotel("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void Eliminar()
        { 
            var idHotel = dtgHoteles.CurrentRow.Cells["idHotel"].Value;
            _hotelesManejador.Eliminar(Convert.ToInt32(idHotel));
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            txtNombreHotel.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            try
            {
                    GuardarHoteles();
                    limpiarcuadros();
                    BuscarHotel(" ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            limpiarcuadros();
        }
        private void GuardarHoteles()
        {
            _hotelesManejador.Guardar(new Hoteles
            {
                idHotel = Convert.ToInt32(lblContador.Text),
                nombreH = txtNombreHotel.Text,
                direccion = txtDireccion.Text,
                telefono = txtTelefono.Text
            });
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarHotel(txtBuscar.Text);
        }
        private void ModificarHotel()
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            lblContador.Text = dtgHoteles.CurrentRow.Cells["idHotel"].Value.ToString();
            txtNombreHotel.Text = dtgHoteles.CurrentRow.Cells["nombreH"].Value.ToString();
            txtDireccion.Text = dtgHoteles.CurrentRow.Cells["direccion"].Value.ToString();
            txtTelefono.Text = dtgHoteles.CurrentRow.Cells["telefono"].Value.ToString();
        }

        private void dtgHoteles_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarHotel();
                BuscarHotel(" ");
            }
            catch (Exception es)
            {

                MessageBox.Show(es.Message);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}