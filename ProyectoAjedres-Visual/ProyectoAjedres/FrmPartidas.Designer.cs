﻿namespace ProyectoAjedres
{
    partial class FrmPartidas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPartidas));
            this.lblContador = new System.Windows.Forms.Label();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.lblBuscar = new System.Windows.Forms.Label();
            this.lblJornada = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNumMovimientos = new System.Windows.Forms.Label();
            this.lblFkJugador = new System.Windows.Forms.Label();
            this.lblFkArbitro = new System.Windows.Forms.Label();
            this.lblFkSala = new System.Windows.Forms.Label();
            this.dtgPartidas = new System.Windows.Forms.DataGridView();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.txtJornada = new System.Windows.Forms.TextBox();
            this.txtEntradasVendidas = new System.Windows.Forms.TextBox();
            this.txtNumMovimientos = new System.Windows.Forms.TextBox();
            this.cmbFkJugador = new System.Windows.Forms.ComboBox();
            this.cmbFkArbitro = new System.Windows.Forms.ComboBox();
            this.cmbFkSala = new System.Windows.Forms.ComboBox();
            this.btnSalir = new System.Windows.Forms.Button();
            this.lblJugador2 = new System.Windows.Forms.Label();
            this.cmbJugador2 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPartidas)).BeginInit();
            this.SuspendLayout();
            // 
            // lblContador
            // 
            this.lblContador.AutoSize = true;
            this.lblContador.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContador.Location = new System.Drawing.Point(698, 13);
            this.lblContador.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblContador.Name = "lblContador";
            this.lblContador.Size = new System.Drawing.Size(18, 19);
            this.lblContador.TabIndex = 8;
            this.lblContador.Text = "0";
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(156, 10);
            this.txtBuscar.Margin = new System.Windows.Forms.Padding(2);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(510, 20);
            this.txtBuscar.TabIndex = 7;
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // lblBuscar
            // 
            this.lblBuscar.AutoSize = true;
            this.lblBuscar.Location = new System.Drawing.Point(67, 17);
            this.lblBuscar.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Size = new System.Drawing.Size(51, 13);
            this.lblBuscar.TabIndex = 6;
            this.lblBuscar.Text = "BUSCAR";
            // 
            // lblJornada
            // 
            this.lblJornada.AutoSize = true;
            this.lblJornada.Location = new System.Drawing.Point(11, 61);
            this.lblJornada.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblJornada.Name = "lblJornada";
            this.lblJornada.Size = new System.Drawing.Size(58, 13);
            this.lblJornada.TabIndex = 10;
            this.lblJornada.Text = "JORNADA";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 108);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "ENTRADAS VENDIDAS";
            // 
            // lblNumMovimientos
            // 
            this.lblNumMovimientos.AutoSize = true;
            this.lblNumMovimientos.Location = new System.Drawing.Point(12, 157);
            this.lblNumMovimientos.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNumMovimientos.Name = "lblNumMovimientos";
            this.lblNumMovimientos.Size = new System.Drawing.Size(111, 13);
            this.lblNumMovimientos.TabIndex = 12;
            this.lblNumMovimientos.Text = "NUM.MOVIMIENTOS";
            // 
            // lblFkJugador
            // 
            this.lblFkJugador.AutoSize = true;
            this.lblFkJugador.Location = new System.Drawing.Point(380, 58);
            this.lblFkJugador.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFkJugador.Name = "lblFkJugador";
            this.lblFkJugador.Size = new System.Drawing.Size(75, 13);
            this.lblFkJugador.TabIndex = 13;
            this.lblFkJugador.Text = "FK JUGADOR";
            // 
            // lblFkArbitro
            // 
            this.lblFkArbitro.AutoSize = true;
            this.lblFkArbitro.Location = new System.Drawing.Point(380, 127);
            this.lblFkArbitro.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFkArbitro.Name = "lblFkArbitro";
            this.lblFkArbitro.Size = new System.Drawing.Size(71, 13);
            this.lblFkArbitro.TabIndex = 14;
            this.lblFkArbitro.Text = "FK ARBITRO";
            // 
            // lblFkSala
            // 
            this.lblFkSala.AutoSize = true;
            this.lblFkSala.Location = new System.Drawing.Point(380, 173);
            this.lblFkSala.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFkSala.Name = "lblFkSala";
            this.lblFkSala.Size = new System.Drawing.Size(50, 13);
            this.lblFkSala.TabIndex = 15;
            this.lblFkSala.Text = "FK SALA";
            // 
            // dtgPartidas
            // 
            this.dtgPartidas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPartidas.Location = new System.Drawing.Point(11, 229);
            this.dtgPartidas.Margin = new System.Windows.Forms.Padding(2);
            this.dtgPartidas.Name = "dtgPartidas";
            this.dtgPartidas.RowTemplate.Height = 24;
            this.dtgPartidas.Size = new System.Drawing.Size(715, 240);
            this.dtgPartidas.TabIndex = 16;
            this.dtgPartidas.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgPartidas_CellDoubleClick);
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackgroundImage = global::ProyectoAjedres.Properties.Resources.save;
            this.btnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGuardar.Location = new System.Drawing.Point(369, 524);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(2);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(61, 41);
            this.btnGuardar.TabIndex = 44;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackgroundImage = global::ProyectoAjedres.Properties.Resources.Cancelar;
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelar.Location = new System.Drawing.Point(483, 524);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(58, 41);
            this.btnCancelar.TabIndex = 43;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackgroundImage = global::ProyectoAjedres.Properties.Resources.eliminar;
            this.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminar.Location = new System.Drawing.Point(593, 524);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(2);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(57, 41);
            this.btnEliminar.TabIndex = 42;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.BackgroundImage = global::ProyectoAjedres.Properties.Resources.Nuevo;
            this.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNuevo.Location = new System.Drawing.Point(269, 524);
            this.btnNuevo.Margin = new System.Windows.Forms.Padding(2);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(63, 41);
            this.btnNuevo.TabIndex = 41;
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // txtJornada
            // 
            this.txtJornada.Location = new System.Drawing.Point(97, 58);
            this.txtJornada.Margin = new System.Windows.Forms.Padding(2);
            this.txtJornada.Name = "txtJornada";
            this.txtJornada.Size = new System.Drawing.Size(249, 20);
            this.txtJornada.TabIndex = 46;
            // 
            // txtEntradasVendidas
            // 
            this.txtEntradasVendidas.Location = new System.Drawing.Point(156, 105);
            this.txtEntradasVendidas.Margin = new System.Windows.Forms.Padding(2);
            this.txtEntradasVendidas.Name = "txtEntradasVendidas";
            this.txtEntradasVendidas.Size = new System.Drawing.Size(190, 20);
            this.txtEntradasVendidas.TabIndex = 47;
            // 
            // txtNumMovimientos
            // 
            this.txtNumMovimientos.Location = new System.Drawing.Point(143, 157);
            this.txtNumMovimientos.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumMovimientos.Name = "txtNumMovimientos";
            this.txtNumMovimientos.Size = new System.Drawing.Size(203, 20);
            this.txtNumMovimientos.TabIndex = 48;
            // 
            // cmbFkJugador
            // 
            this.cmbFkJugador.FormattingEnabled = true;
            this.cmbFkJugador.Location = new System.Drawing.Point(483, 53);
            this.cmbFkJugador.Margin = new System.Windows.Forms.Padding(2);
            this.cmbFkJugador.Name = "cmbFkJugador";
            this.cmbFkJugador.Size = new System.Drawing.Size(183, 21);
            this.cmbFkJugador.TabIndex = 49;
            this.cmbFkJugador.Click += new System.EventHandler(this.cmbFkJugador_Click);
            // 
            // cmbFkArbitro
            // 
            this.cmbFkArbitro.FormattingEnabled = true;
            this.cmbFkArbitro.Location = new System.Drawing.Point(483, 124);
            this.cmbFkArbitro.Margin = new System.Windows.Forms.Padding(2);
            this.cmbFkArbitro.Name = "cmbFkArbitro";
            this.cmbFkArbitro.Size = new System.Drawing.Size(183, 21);
            this.cmbFkArbitro.TabIndex = 50;
            this.cmbFkArbitro.Click += new System.EventHandler(this.cmbFkArbitro_Click);
            // 
            // cmbFkSala
            // 
            this.cmbFkSala.FormattingEnabled = true;
            this.cmbFkSala.Location = new System.Drawing.Point(483, 165);
            this.cmbFkSala.Margin = new System.Windows.Forms.Padding(2);
            this.cmbFkSala.Name = "cmbFkSala";
            this.cmbFkSala.Size = new System.Drawing.Size(183, 21);
            this.cmbFkSala.TabIndex = 51;
            this.cmbFkSala.Click += new System.EventHandler(this.cmbFkSala_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.Location = new System.Drawing.Point(173, 525);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(63, 40);
            this.btnSalir.TabIndex = 52;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblJugador2
            // 
            this.lblJugador2.AutoSize = true;
            this.lblJugador2.Location = new System.Drawing.Point(380, 90);
            this.lblJugador2.Name = "lblJugador2";
            this.lblJugador2.Size = new System.Drawing.Size(67, 13);
            this.lblJugador2.TabIndex = 53;
            this.lblJugador2.Text = "FK Jugador2";
            // 
            // cmbJugador2
            // 
            this.cmbJugador2.FormattingEnabled = true;
            this.cmbJugador2.Location = new System.Drawing.Point(483, 87);
            this.cmbJugador2.Name = "cmbJugador2";
            this.cmbJugador2.Size = new System.Drawing.Size(183, 21);
            this.cmbJugador2.TabIndex = 54;
            this.cmbJugador2.Click += new System.EventHandler(this.cmbJugador2_Click);
            // 
            // FrmPartidas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProyectoAjedres.Properties.Resources.fondos_gratis;
            this.ClientSize = new System.Drawing.Size(751, 566);
            this.Controls.Add(this.cmbJugador2);
            this.Controls.Add(this.lblJugador2);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.cmbFkSala);
            this.Controls.Add(this.cmbFkArbitro);
            this.Controls.Add(this.cmbFkJugador);
            this.Controls.Add(this.txtNumMovimientos);
            this.Controls.Add(this.txtEntradasVendidas);
            this.Controls.Add(this.txtJornada);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.dtgPartidas);
            this.Controls.Add(this.lblFkSala);
            this.Controls.Add(this.lblFkArbitro);
            this.Controls.Add(this.lblFkJugador);
            this.Controls.Add(this.lblNumMovimientos);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblJornada);
            this.Controls.Add(this.lblContador);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.lblBuscar);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmPartidas";
            this.Text = "´´";
            this.Load += new System.EventHandler(this.FrmPartidas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPartidas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblContador;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Label lblBuscar;
        private System.Windows.Forms.Label lblJornada;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblNumMovimientos;
        private System.Windows.Forms.Label lblFkJugador;
        private System.Windows.Forms.Label lblFkArbitro;
        private System.Windows.Forms.Label lblFkSala;
        private System.Windows.Forms.DataGridView dtgPartidas;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.TextBox txtJornada;
        private System.Windows.Forms.TextBox txtEntradasVendidas;
        private System.Windows.Forms.TextBox txtNumMovimientos;
        private System.Windows.Forms.ComboBox cmbFkJugador;
        private System.Windows.Forms.ComboBox cmbFkArbitro;
        private System.Windows.Forms.ComboBox cmbFkSala;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label lblJugador2;
        private System.Windows.Forms.ComboBox cmbJugador2;
    }
}