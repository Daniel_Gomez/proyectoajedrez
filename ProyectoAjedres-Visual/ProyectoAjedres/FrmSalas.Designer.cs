﻿namespace ProyectoAjedres
{
    partial class FrmSalas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSalas));
            this.lblContador = new System.Windows.Forms.Label();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.lblBuscar = new System.Windows.Forms.Label();
            this.txtMedios = new System.Windows.Forms.TextBox();
            this.txtCapacidad = new System.Windows.Forms.TextBox();
            this.txtNumSala = new System.Windows.Forms.TextBox();
            this.lblMedios = new System.Windows.Forms.Label();
            this.lblCapacidad = new System.Windows.Forms.Label();
            this.lblNumSala = new System.Windows.Forms.Label();
            this.cmbFkHotel = new System.Windows.Forms.ComboBox();
            this.lblFkHotel = new System.Windows.Forms.Label();
            this.dtgSalas = new System.Windows.Forms.DataGridView();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgSalas)).BeginInit();
            this.SuspendLayout();
            // 
            // lblContador
            // 
            this.lblContador.AutoSize = true;
            this.lblContador.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContador.Location = new System.Drawing.Point(637, 21);
            this.lblContador.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblContador.Name = "lblContador";
            this.lblContador.Size = new System.Drawing.Size(18, 19);
            this.lblContador.TabIndex = 11;
            this.lblContador.Text = "0";
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(115, 14);
            this.txtBuscar.Margin = new System.Windows.Forms.Padding(2);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(494, 20);
            this.txtBuscar.TabIndex = 10;
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // lblBuscar
            // 
            this.lblBuscar.AutoSize = true;
            this.lblBuscar.Location = new System.Drawing.Point(32, 17);
            this.lblBuscar.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Size = new System.Drawing.Size(51, 13);
            this.lblBuscar.TabIndex = 9;
            this.lblBuscar.Text = "BUSCAR";
            // 
            // txtMedios
            // 
            this.txtMedios.Location = new System.Drawing.Point(429, 65);
            this.txtMedios.Margin = new System.Windows.Forms.Padding(2);
            this.txtMedios.Name = "txtMedios";
            this.txtMedios.Size = new System.Drawing.Size(198, 20);
            this.txtMedios.TabIndex = 35;
            // 
            // txtCapacidad
            // 
            this.txtCapacidad.Location = new System.Drawing.Point(100, 106);
            this.txtCapacidad.Margin = new System.Windows.Forms.Padding(2);
            this.txtCapacidad.Name = "txtCapacidad";
            this.txtCapacidad.Size = new System.Drawing.Size(222, 20);
            this.txtCapacidad.TabIndex = 34;
            // 
            // txtNumSala
            // 
            this.txtNumSala.Location = new System.Drawing.Point(100, 68);
            this.txtNumSala.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumSala.Name = "txtNumSala";
            this.txtNumSala.Size = new System.Drawing.Size(222, 20);
            this.txtNumSala.TabIndex = 33;
            // 
            // lblMedios
            // 
            this.lblMedios.AutoSize = true;
            this.lblMedios.Location = new System.Drawing.Point(348, 68);
            this.lblMedios.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMedios.Name = "lblMedios";
            this.lblMedios.Size = new System.Drawing.Size(49, 13);
            this.lblMedios.TabIndex = 31;
            this.lblMedios.Text = "MEDIOS";
            // 
            // lblCapacidad
            // 
            this.lblCapacidad.AutoSize = true;
            this.lblCapacidad.Location = new System.Drawing.Point(15, 106);
            this.lblCapacidad.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCapacidad.Name = "lblCapacidad";
            this.lblCapacidad.Size = new System.Drawing.Size(68, 13);
            this.lblCapacidad.TabIndex = 30;
            this.lblCapacidad.Text = "CAPACIDAD";
            // 
            // lblNumSala
            // 
            this.lblNumSala.AutoSize = true;
            this.lblNumSala.Location = new System.Drawing.Point(21, 68);
            this.lblNumSala.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNumSala.Name = "lblNumSala";
            this.lblNumSala.Size = new System.Drawing.Size(62, 13);
            this.lblNumSala.TabIndex = 29;
            this.lblNumSala.Text = "NUM.SALA";
            // 
            // cmbFkHotel
            // 
            this.cmbFkHotel.FormattingEnabled = true;
            this.cmbFkHotel.Location = new System.Drawing.Point(429, 103);
            this.cmbFkHotel.Margin = new System.Windows.Forms.Padding(2);
            this.cmbFkHotel.Name = "cmbFkHotel";
            this.cmbFkHotel.Size = new System.Drawing.Size(198, 21);
            this.cmbFkHotel.TabIndex = 53;
            this.cmbFkHotel.Click += new System.EventHandler(this.cmbFkHotel_Click);
            // 
            // lblFkHotel
            // 
            this.lblFkHotel.AutoSize = true;
            this.lblFkHotel.Location = new System.Drawing.Point(348, 106);
            this.lblFkHotel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFkHotel.Name = "lblFkHotel";
            this.lblFkHotel.Size = new System.Drawing.Size(59, 13);
            this.lblFkHotel.TabIndex = 52;
            this.lblFkHotel.Text = "FK HOTEL";
            // 
            // dtgSalas
            // 
            this.dtgSalas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgSalas.Location = new System.Drawing.Point(35, 136);
            this.dtgSalas.Margin = new System.Windows.Forms.Padding(2);
            this.dtgSalas.Name = "dtgSalas";
            this.dtgSalas.RowTemplate.Height = 24;
            this.dtgSalas.Size = new System.Drawing.Size(641, 190);
            this.dtgSalas.TabIndex = 54;
            this.dtgSalas.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgSalas_CellDoubleClick);
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackgroundImage = global::ProyectoAjedres.Properties.Resources.save;
            this.btnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGuardar.Location = new System.Drawing.Point(363, 330);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(2);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(61, 41);
            this.btnGuardar.TabIndex = 58;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackgroundImage = global::ProyectoAjedres.Properties.Resources.Cancelar;
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelar.Location = new System.Drawing.Point(459, 330);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(58, 41);
            this.btnCancelar.TabIndex = 57;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackgroundImage = global::ProyectoAjedres.Properties.Resources.eliminar;
            this.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminar.Location = new System.Drawing.Point(552, 330);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(2);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(57, 41);
            this.btnEliminar.TabIndex = 56;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.BackgroundImage = global::ProyectoAjedres.Properties.Resources.Nuevo;
            this.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNuevo.Location = new System.Drawing.Point(271, 330);
            this.btnNuevo.Margin = new System.Windows.Forms.Padding(2);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(63, 41);
            this.btnNuevo.TabIndex = 55;
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.Location = new System.Drawing.Point(186, 330);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(63, 39);
            this.btnSalir.TabIndex = 59;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // FrmSalas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProyectoAjedres.Properties.Resources.fondos_gratis;
            this.ClientSize = new System.Drawing.Size(687, 382);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.dtgSalas);
            this.Controls.Add(this.cmbFkHotel);
            this.Controls.Add(this.lblFkHotel);
            this.Controls.Add(this.txtMedios);
            this.Controls.Add(this.txtCapacidad);
            this.Controls.Add(this.txtNumSala);
            this.Controls.Add(this.lblMedios);
            this.Controls.Add(this.lblCapacidad);
            this.Controls.Add(this.lblNumSala);
            this.Controls.Add(this.lblContador);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.lblBuscar);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmSalas";
            this.Text = "Salas";
            this.Load += new System.EventHandler(this.FrmSalas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgSalas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblContador;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Label lblBuscar;
        private System.Windows.Forms.TextBox txtMedios;
        private System.Windows.Forms.TextBox txtCapacidad;
        private System.Windows.Forms.TextBox txtNumSala;
        private System.Windows.Forms.Label lblMedios;
        private System.Windows.Forms.Label lblCapacidad;
        private System.Windows.Forms.Label lblNumSala;
        private System.Windows.Forms.ComboBox cmbFkHotel;
        private System.Windows.Forms.Label lblFkHotel;
        private System.Windows.Forms.DataGridView dtgSalas;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnSalir;
    }
}