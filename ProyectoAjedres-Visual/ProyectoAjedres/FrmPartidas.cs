﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ProyectoAjedres;
using LogicaNegocio.ProyectoAjedres;
using Microsoft.CSharp.RuntimeBinder;

namespace ProyectoAjedres
{
    public partial class FrmPartidas : Form
    {
        private SalasManejador _salasManejador;
        private ArbitrosManejador _arbitrosManejador;
        private JugadoresManejador _jugadoresManejador;
        private PartidasManejador _partidasManejador;
        private Partidas _partidas;
        public FrmPartidas()
        {
            InitializeComponent();
            _partidas = new Partidas();
            _partidasManejador = new PartidasManejador();
            _jugadoresManejador = new JugadoresManejador();
            _arbitrosManejador = new ArbitrosManejador();
            _salasManejador = new SalasManejador();

        }
        private void BuscarPartidas(string Filtro)
        {
            dtgPartidas.DataSource = _partidasManejador.GetPartidas(Filtro);
        }

        private void FrmPartidas_Load(object sender, EventArgs e)
        {
            BuscarPartidas("");
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }
        
        private void controlarbotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void controlarcuadros(bool activar)
        {
            txtJornada.Enabled = activar;
            txtEntradasVendidas.Enabled = activar;
            txtNumMovimientos.Enabled = activar;
            cmbFkJugador.Enabled = activar;
            cmbJugador2.Enabled = activar;
            cmbFkArbitro.Enabled = activar;
            cmbFkSala.Enabled = activar;
        }
        private void limpiarcuadros()
        {
            txtJornada.Text = "";
            txtEntradasVendidas.Text = "";
            txtNumMovimientos.Text = "";
            cmbFkJugador.Text = "";
            cmbJugador2.Text = "";
            cmbFkArbitro.Text = "";
            cmbFkSala.Text = "";
            lblContador.Text = "0";
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar esta Partida", "Elimirar Partida", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarPartidas("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void Eliminar()
        {
            var CodigoP = dtgPartidas.CurrentRow.Cells["codigoP"].Value;
            _partidasManejador.Eliminar(Convert.ToInt32(CodigoP));
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            txtJornada.Focus();
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            try
            {
                    GuardarPartidas();
                    limpiarcuadros();
                    BuscarPartidas(" ");
                    controlarbotones(true, false, false, true);
                    controlarcuadros(false);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            limpiarcuadros();
        }
        private void GuardarPartidas()
        {
            _partidasManejador.Guardar(new Partidas
            {
                CodigoP = Convert.ToInt32(lblContador.Text),
                Jornada = txtJornada.Text,
                EntradasVendidas = int.Parse(txtEntradasVendidas.Text),
                NumMovimientos = int.Parse(txtNumMovimientos.Text),
                fkidJugador = int.Parse(cmbFkJugador.SelectedValue.ToString()),
                fkidJugador2 = int.Parse(cmbJugador2.SelectedValue.ToString()),
                fkidArbitro = int.Parse(cmbFkArbitro.SelectedValue.ToString()),
                fkidSala = int.Parse(cmbFkSala.SelectedValue.ToString())

            });
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarPartidas(txtBuscar.Text);
        }
        private void ModificarPartida()
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            lblContador.Text = dtgPartidas.CurrentRow.Cells["codigoP"].Value.ToString();
            txtJornada.Text = dtgPartidas.CurrentRow.Cells["jornada"].Value.ToString();
            txtEntradasVendidas.Text = dtgPartidas.CurrentRow.Cells["entradasvendidas"].Value.ToString();
            txtNumMovimientos.Text = dtgPartidas.CurrentRow.Cells["NumMovimientos"].Value.ToString();
            cmbFkJugador.Text = dtgPartidas.CurrentRow.Cells["fkidJugador"].Value.ToString();
            cmbJugador2.Text = dtgPartidas.CurrentRow.Cells["fkidJugador2"].Value.ToString();
            cmbFkArbitro.Text = dtgPartidas.CurrentRow.Cells["fkidArbitro"].Value.ToString();
            cmbFkSala.Text = dtgPartidas.CurrentRow.Cells["fkidSala"].Value.ToString();
        }

        private void dtgPartidas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarPartida();
                BuscarPartidas(" ");
            }
            catch (Exception es)
            {

                MessageBox.Show(es.Message);
            }
        }
        private void extraerJugadores(string Filtro)
        {
            cmbFkJugador.DataSource = _jugadoresManejador.GetJugadores(Filtro);
            cmbFkJugador.ValueMember = "idJugador";
            cmbFkJugador.DisplayMember = "nombreJ";
        }
        private void eztraerJugadores(string Filtro)
        { 
            cmbJugador2.DataSource = _jugadoresManejador.GetJugadores(Filtro);
            cmbJugador2.ValueMember = "idJugador";
            cmbJugador2.DisplayMember = "nombreJ";
        }
        
        private void cmbFkJugador_Click(object sender, EventArgs e)
        {
            extraerJugadores("");
        }
        private void cmbJugador2_Click(object sender, EventArgs e)
        {
            eztraerJugadores("");
        }
        private void ExtraerArbitros(string Filtro)
        {
            cmbFkArbitro.DataSource = _arbitrosManejador.GetArbitros(Filtro);
            cmbFkArbitro.ValueMember = "idArbitro";
            cmbFkArbitro.DisplayMember = "nombreA";
        }
        private void cmbFkArbitro_Click(object sender, EventArgs e)
        {
            ExtraerArbitros("");
        }
        private void extraerSalas(string Filtro)
        {
            cmbFkSala.DataSource = _salasManejador.GetSalas(Filtro);
            cmbFkSala.ValueMember = "idSala";
            cmbFkSala.DisplayMember = "numSala";
        }
        private void cmbFkSala_Click(object sender, EventArgs e)
        {
            extraerSalas("");
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
