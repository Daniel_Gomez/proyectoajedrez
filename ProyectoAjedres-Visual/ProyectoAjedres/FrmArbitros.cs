﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ProyectoAjedres;
using LogicaNegocio.ProyectoAjedres;

namespace ProyectoAjedres
{
    public partial class FrmArbitros : Form
    {
        private Arbitros_VManejador arbitros_VManejador;
        private PaisesManejador _paisesManejador;
        private ArbitrosManejador _arbitrosManejador;
        private Arbitros arbitros;
        public FrmArbitros()
        {
            InitializeComponent();
            arbitros = new Arbitros();
            arbitros_VManejador = new Arbitros_VManejador();
            arbitros = new Arbitros();
            _arbitrosManejador = new ArbitrosManejador();
            _paisesManejador = new PaisesManejador();
        }
        private void BuscarArbitro(string Filtro)
        {
            dtgArbitros.DataSource = _arbitrosManejador.GetArbitros(Filtro);
            dtgArbitros.DataSource = arbitros_VManejador.GetArbitros_Vs(Filtro);
        }

        private void FrmArbitros_Load(object sender, EventArgs e)
        {
            BuscarArbitro("");
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }
        private void controlarbotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void controlarcuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtDireccion.Enabled = activar;
            txtTelefono.Enabled = activar;
            txtCampeonatos.Enabled = activar;
            cmbFkPais.Enabled = activar;
        }
        private void limpiarcuadros()
        {
            txtNombre.Text = "";
            txtDireccion.Text = "";
            txtTelefono.Text = "";
            txtCampeonatos.Text = "";
            cmbFkPais.Text = "";
            lblContador.Text = "0";
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este arbitro", "Elimirar arbitro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarArbitro("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void Eliminar()
        {
            var idArbitro = dtgArbitros.CurrentRow.Cells["idArbitro"].Value;
            _arbitrosManejador.Eliminar(Convert.ToInt32(idArbitro));
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            txtNombre.Focus();
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            try
            {
                    GuardarArbitros();
                    limpiarcuadros();
                    BuscarArbitro("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            limpiarcuadros();
        }
        private void GuardarArbitros()
        {
            _arbitrosManejador.Guardar(new Arbitros
            {
                idArbitro = Convert.ToInt32(lblContador.Text),
                nombreA = txtNombre.Text,
                direccion = txtDireccion.Text,
                telefono = txtTelefono.Text,
                campeonato = int.Parse(txtCampeonatos.Text),
                fkidPais = int.Parse(cmbFkPais.SelectedValue.ToString())
            });
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarArbitro(txtBuscar.Text);
        }
        private void ModificarArbitro()
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            lblContador.Text = dtgArbitros.CurrentRow.Cells["idArbitro"].Value.ToString();
            txtNombre.Text = dtgArbitros.CurrentRow.Cells["nombreA"].Value.ToString();
            txtDireccion.Text = dtgArbitros.CurrentRow.Cells["direccion"].Value.ToString();
            txtTelefono.Text = dtgArbitros.CurrentRow.Cells["telefono"].Value.ToString();
            txtCampeonatos.Text = dtgArbitros.CurrentRow.Cells["campeonatos"].Value.ToString();
            cmbFkPais.Text = dtgArbitros.CurrentRow.Cells["fkidPais"].Value.ToString();
        }

        private void dtgArbitros_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                ModificarArbitro();
                BuscarArbitro(" ");
            }
            catch (Exception es)
            {

                MessageBox.Show(es.Message);
            }
        }
        private void extraerPaises(string Filtro)
        {
            cmbFkPais.DataSource = _paisesManejador.GetPaises(Filtro);
            cmbFkPais.ValueMember = "idPais";
            cmbFkPais.DisplayMember = "nombreP";
        }

        private void cmbFkPais_Click(object sender, EventArgs e)
        {
            extraerPaises("");
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
