create databases ClubAjedres;
use clubajedres;

create table jugadores(idJugador int primary key AUTO_INCREMENT, nombreJ varchar (50), direccion varchar(50),telefono varchar(50),
campeonatos int,nivel int,fkidPais int,foreign key (fkidPais) references paises(idPais));

create table arbitros(idArbitro int primary key AUTO_INCREMENT, nombreA varchar (50), direccion varchar (50),telefono varchar(50),
campeonatos int,fkidPais int,foreign key (fkidPais) references paises(idPais));

create table paises(idPais int primary key AUTO_INCREMENT,nombreP varchar(50),numClubes int);

create table  partidas(codigoP int primary key AUTO_INCREMENT, jornada  varchar(50),entradasvendidas int, numeromovimientos int,
fkidJugador int,fkidJugador2 int,fkidArbitro int,fkidSala int, foreign key (fkidJugador) references jugadores(idJugador),
foreign key (fkidJugador2) references jugadores(idJugador),foreign key (fkidArbitro) references arbitros(idArbitro),
foreign key (fkidSala) references salas(idSala));

create table salas(idSala int primary key AUTO_INCREMENT,numSala varchar (50), Capacidad  int,medios varchar (50),fkidHotel int,
foreign key (fkidHotel) references hoteles(idHotel));

create table hoteles(idHotel int primary key AUTO_INCREMENT, nombreH varchar (50),direccion varchar  (50),telefono varchar(50));

# region procedimientos almacenados 
 /*
# region Jugadores



drop procedure if exists ModificarJugadores;
create procedure ModificarJugadores(
in _idJugador int,
  in _nombreJ varchar(50),
  in _direccion varchar (50),
  in _telefono varchar (50),
  in _campeonatos int,
  in _nivel int,
  in _fkidpais int
)
begin 
update jugadores set nombreJ = _nombreJ,direccion = _direccion,telefono = _telefono, campeonatos = _campeonatos,nivel = _nivel,
fkidpais =_fkidpais where idJugador = _idJugador;
select "registro Actualizado Correctamente";
end;
call ModificarJugadores(3,'joaquin','sinaloa','4745696747',1,8,2);


#end region

#region Arbitros


call InsertarArbitros(2,'Tadashi','nagasaki','6754093243',3,2);

drop procedure if exists ModificarArbitros;
create procedure ModificarArbitros(
  in _idArbitro int,
  in _nombreA varchar(50),
  in _direccion varchar (50),
  in _telefono varchar (50),
  in _campeonatos int,
  in _fkidPais int
)
begin 
update arbitros set nombreA = _nombreA,direccion = _direccion,telefono = _telefono, campeonatos = _campeonatos,
fkidpais =_fkidpais where idArbitro = _idArbitro;
select "registro Actualizado Correctamente";
end;
call ModificarArbitros(2,'Tadashi','nagasaki','6754093243',3,2);


#end region

# region partidas



drop procedure if exists ModificarPartidas;
create procedure ModificarPartidas(
  in _codigoP int,
  in _jornada varchar (50),
  in _entradasvendidas int,
  in _numeromovimientos int,
  in _Blancas int,
  in _Negras int,
  in _fkidArbitro int,
  in _fkidSala int
)
begin 
update partidas set jornada = _jornada,entradasvendidas = _entradasvendidas,numeromovimientos = _numeromovimientos,
fkidJugador = _Blancas,fkidJugador2 = _Negras,fkidArbitro = _fkidArbitro,fkidSala = _fkidSala where codigoP = _codigoP;
select "registro Actualizado Correctamente";
end;
call ModificarPartidas(1,'primera',50,15,2,4,3,4);


#end region

# region Paises


drop procedure if exists Modificarpais;
create procedure Modificarpais(
in _idPais int,
in _nombreP varchar (50),
in _numClubes int
)
begin
update paises set nombreP = _nombreP, numClubes = _numClubes where idPais = _idPais;
select "registro actualizado con exito";
end;
call Modificarpais(1,'Mexico',13);


# end region

#region salas


drop procedure if exists ModificarSala;
create procedure ModificarSala(
in _idSala int,
in _numSala varchar (50),
in _Capacidad int,
in _medios varchar (50),
in _fkidHotel int
)
begin
update salas set numSala = _numSala, Capacidad = _Capacidad, medios = _medios where fkidHotel = _fkidHotel;
select "Registro Actualizado con exito";
end;
call ModificarSala(1,'Sala1',50,'Camaras de video',2);

# end region

#region hoteles



drop procedure if exists Modificarhoteles;
create procedure Modificarhoteles(
 in _idHotel int,
 in _nombreH varchar(50),
 in _direccion varchar(50),
 in _telefono varchar (50)
)
 begin
 update hoteles set nombreH = _nombreH, direccion = _direccion, telefono = _telefono where idHotel = _idHotel;
 select "registro actualizado y guardado";
 end;
 call Modificarhoteles (1,'LasFuentes','alcaldes','4741001030');
 
 
 
 
# end region */
#end Region

# region consultas
#region inner join
select t1.*,t2.*
from hoteles t1
inner join
salas t2
on t1.idHotel = t2.idSala;

select t1.*,t3.*
from hoteles t1
inner join
partidas t3
on t1.idHotel = t3.codigoP;

select t2.*,t3.*
from salas t2
inner join
partidas t3 
on t2.idSala = t3.codigoP;

select t1.*,t2.*
from jugadores t1
inner join
arbitros t2
on t1.idJugador = t2.idArbitro;

select t1.*,t2.*
from jugadores t1
inner join
paises t2
on t1.idJugador = t2.idPais;

select t1.*,t2.*
from arbitros t1
inner join
paises t2
on t1.idArbitro = t2.idPais;

select t4.*,t5.*
from jugadores t4
inner join
arbitros t5
on t4.idJugador = t5.idArbitro;

select t4.*,t6.*
from jugadores t4
inner join
paises t6
on t4.idJugador = t6.idPais;

select t5.*,t6.*
from arbitros t5
inner join
paises t6
on t5.idArbitro = t6.idPais;
#end region
#region left join
select t1.*,t2.*
from hoteles t1
left join salas t2
on t1.idHotel = t2.idSala;

select t1.*,t3.*
from hoteles t1 
left join partidas t3
on t1.idHotel = t3.codigoP;

select t2.*,t3.*
from salas t2
left join partidas t3
on t2.idSala = t3.codigoP;

select t1.*,t2.*
from jugadores t1
left join
arbitros t2
on t1.idJugador = t2.idArbitro;

select t1.*,t2.*
from jugadores t1
left join
paises t2
on t1.idJugador = t2.idPais;

select t1.*,t2.*
from arbitros t1
left join
paises t2
on t1.idArbitro = t2.idPais;

select t4.*,t5.*
from jugadores t4
left join
arbitros t5
on t4.idJugador = t5.idArbitro;

select t4.*,t6.*
from jugadores t4
left join
paises t6
on t4.idJugador = t6.idPais;

select t5.*,t6.*
from arbitros t5
left join
paises t6
on t5.idArbitro = t6.idPais;

#end region 
#region right join

select t1.*,t2.*
from hoteles t1
right join salas t2
on t1.idHotel = t2.idSala;

 select t1.*, t3.*
 from hoteles t1
 right join partidas t3
 on t1.idHotel = t3.codigoP;
 
 select t2.*,t3.*
 from salas t2 
 right join partidas t3
 on t2.idSala = t3.codigoP;
 

 select t1.*,t2.*
from jugadores t1
right join
arbitros t2
on t1.idJugador = t2.idArbitro;

select t1.*,t2.*
from jugadores t1
right join
paises t2
on t1.idJugador = t2.idPais;

select t1.*,t2.*
from arbitros t1
right join
paises t2
on t1.idArbitro = t2.idPais;

 select t4.*,t5.*
from jugadores t4
right  join
arbitros t5
on t4.idJugador = t5.idArbitro;

select t4.*,t6.*
from jugadores t4
right  join
paises t6
on t4.idJugador = t6.idPais;

select t5.*,t6.*
from arbitros t5
right  join
paises t6
on t5.idArbitro = t6.idPais;

#end region
#region outerjoin
select t1.*,t2.*
from hoteles t1
left join salas t2
on t1.idHotel = t2.idSala
union
select t1.*,t2.*
from hoteles t1
right join salas t2
on t1.idHotel = t2.idSala;

select t1.*,t3.*
from hoteles t1 
left join partidas t3
on t1.idHotel = t3.codigoP
union
 select t1.*, t3.*
 from hoteles t1
 right join partidas t3
 on t1.idHotel = t3.codigoP;
 
 select t2.*,t3.*
from salas t2
left join partidas t3
on t2.idSala = t3.codigoP
 union
  select t2.*,t3.*
 from salas t2 
 right join partidas t3
 on t2.idSala = t3.codigoP;
 
 select t4.*,t5.*
from jugadores t4
left join
arbitros t5
on t4.idJugador = t5.idArbitro
 union
  select t4.*,t5.*
from jugadores t4
right  join
arbitros t5
on t4.idJugador = t5.idArbitro;

select t4.*,t6.*
from jugadores t4
left join
paises t6
on t4.idJugador = t6.idPais
union
select t4.*,t6.*
from jugadores t4
right  join
paises t6
on t4.idJugador = t6.idPais;

select t5.*,t6.*
from arbitros t5
left join
paises t6
on t5.idArbitro = t6.idPais
union
select t5.*,t6.*
from arbitros t5
right  join
paises t6
on t5.idArbitro = t6.idPais;
#end region
# region crossjoin

select t1.*,t2.*
from hoteles t1
cross join
salas t2
on t1.nombreH = 'casagrande';

select t1.*,t3.*
from hoteles t1
cross join
partidas t3
on  t3.entradasvendidas = 50;

select t2.*,t3.*
from salas t2
cross join
partidas t3 
on t2.capacidad = 70;

select t4.*,t5.*
from jugadores t4
cross join
arbitros t5
on t4.nivel = 8;

select t4.*,t6.*
from jugadores t4
cross join
paises t6
on t6.nombreP = 'Mexico'; 

select t5.*,t6.*
from arbitros t5
cross join
paises t6
on t5.NombreA = 'Dante';
# end region

# end region

#region vistas
 create view Partida_V as
 select jornada, entradasvendidas, numeromovimientos, fkidJugador as 'blancas', fkidJugador2 as 'negras',nombreA as 'Arbitro',numSala as 'Sala'
 from partidas,jugadores,arbitros,salas
 where fkidJugador = idJugador and fkidArbitro = idArbitro and fkidSala = idSala;
 

 create view Jugador_V as
 select nombreJ as 'jugador', direccion, telefono, campeonatos, nivel as 'experiencia', fkidpais as 'pais'from
 jugadores, paises where idPais = fkidPais;  
 
 create view Arbitro_V as
 select nombreA as 'arbitro', direccion, telefono, campeonatos, fkidpais as 'pais'from arbitros, paises
 where  idPais = fkidPais;
#end region

#region roles, due�o, admin y user

create user 'Dante'@'localhost' identified by 'Dante';
create user 'Due�o'@'localhost';
create user 'Admin'@'localhost';
/* Things*/
select user from mysql.user;/*para hacer una consulta de los usuarios existentes*/ 
drop user 'Dante'@'localhost'; /*Eliminar un usuario*/
#end region

#region permisos
/*dar permisos*/
grant all privileges on *.* to 'Due�o'@'localhost'; /* todos los permisos de todas las bases de datos y de todas las tablas*/
 grant select, insert,update, create on clubajedres.* to 'Admin'@'localhost';/* permisos de consultar, insertar, actualizar, y crear en la bd ajedres y todas las tablas*/
 grant select, insert on clubajedres.* to 'Dante'@'localhost'; /*solo tiene permiso de consultar y insertar en las tablas de ClubAjedres*/
 /*quitar permisos*/
 REVOKE UPDATE, DELETE ON *.* FROM 'Admin'@'localhost'; /*Eliminar permisos*/
REVOKE ALL PRIVILEGES ON *.* FROM 'Dante'@'localhost'; /*Eliminar todos los permisos*/
#end region
