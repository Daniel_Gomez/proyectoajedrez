﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ProyectoAjedres
{
    public class Jugadores
    {
        private int _IDJugador;
        private string _NombreJ;
        private string _Direccion;
        private string _Telefono;
        private int _Campeonatos;
        private int _Nivel;
        private string _FKIDPais;
       

        public int IDJugador { get => _IDJugador; set => _IDJugador = value; }
        public string NombreJ { get => _NombreJ; set => _NombreJ = value; }
        public string Direccion { get => _Direccion; set => _Direccion = value; }
        public string Telefono{ get => _Telefono; set => _Telefono = value; }
        public int Campeonatos { get => _Campeonatos; set => _Campeonatos = value; }
        public int Nivel { get => _Nivel; set => _Nivel = value; }
        public string fkidPais { get => _FKIDPais; set => _FKIDPais = value; }
    }
}
