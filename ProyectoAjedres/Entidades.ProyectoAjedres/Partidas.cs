﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ProyectoAjedres
{
     public class Partidas
    {
        private int _CodigoP;
        private string _Jornada;
        private int _EntradasVendidas;
        private int _NumMovimientos;
        private int _FKIDJugador;
        private int _FKIDJugador2;
        private int _FKIDArbitro;
        private int _FKIDSala;


        public int CodigoP { get => _CodigoP; set => _CodigoP = value; }
        public string Jornada { get => _Jornada; set => _Jornada = value; }
        public int EntradasVendidas { get => _EntradasVendidas; set => _EntradasVendidas = value; }
        public int NumMovimientos { get => _NumMovimientos; set => _NumMovimientos = value; }
        public int fkidJugador { get => _FKIDJugador; set => _FKIDJugador = value; }
        public int fkidJugador2 { get => _FKIDJugador2; set => _FKIDJugador2 = value; }
        public int fkidArbitro { get => _FKIDArbitro; set => _FKIDArbitro = value; }
        public int fkidSala { get => _FKIDSala; set => _FKIDSala = value; }
    }
}
